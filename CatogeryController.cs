﻿using Microsoft.AspNetCore.Mvc;
using NIMAP_PROJECT.Models;
using System.Data.SqlClient;

namespace NIMAP_PROJECT.Controllers
{
    public class CatogeryController : Controller
    {

        CDatalayer cd = new CDatalayer();
        Models.Catogery c = new Models.Catogery();
        public IActionResult Index()
        {

            List<Models.Catogery> list = new List<Models.Catogery>();
            list = (List<Models.Catogery>)cd.getcatogery();
            return View(list);
        }


        public IActionResult create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult create(Catogery c)
        {
            if (ModelState.IsValid)
            {
                cd.addcatogery(c);
                return RedirectToAction("Index");

            }
            return View(c);
        }
        [HttpGet]
        public IActionResult edit(int? id)
        {
            var se = cd.Getca(id);
            return View(se);

        }
        [HttpPost]
        public IActionResult edit(Catogery ca)
        {
            if (ModelState.IsValid)
            {
                cd.updatecatogery(ca);
                return RedirectToAction("Index");
            }
            return View(ca);

        }

        public IActionResult delete(int? id)
        {
            var se = cd.Getca(id);
            return View(se);
        }
        [HttpPost]
        public IActionResult delete(Catogery ca)
        {
            if (ModelState.IsValid)
            {
                cd.deleteca(ca);
                return RedirectToAction("Index");
            }
            return View(ca);
        }

        private catogerymodel GetCustomers(int currentPage)
        {
            int maxRows = 10;
            catogerymodel ca = new catogerymodel();
            ca.cat=(from i in cd.getcatogery()
                    select i).ToList().OrderBy(X=>X.CategoryId).Skip((currentPage - 1) * maxRows).Take(maxRows).ToList();

            double pageCount = (double)((decimal)cd.getcatogery().Count() / Convert.ToDecimal(maxRows));
            ca.pagecount = (int)Math.Ceiling(pageCount);

            ca.currentpageindex = currentPage;

            return ca;


        }
    }
}
