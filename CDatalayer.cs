﻿using System.Data.SqlClient;

namespace NIMAP_PROJECT.Models
{
    public class CDatalayer
    {
        string con = "Data Source=LAPTOP-IKKBD7PU\\SQLEXPRESS;Initial Catalog=NIMAP;Integrated Security=True";

        public IEnumerable<Catogery> getcatogery()
        {
           List<Catogery> Clist = new List<Catogery>();
            using(SqlConnection conn = new SqlConnection(con))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_show",conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader dr =cmd.ExecuteReader();
                while (dr.Read())
                {
                    Catogery c = new Catogery();
                    c.ProductId = Convert.ToInt32(dr[0]);
                    c.ProductName = dr[1].ToString();
                    c.CategoryId= Convert.ToInt32(dr[2]);
                    c.catogeryName = dr[3].ToString();
                    Clist.Add(c);   
                }

                return Clist;

            }
        }

        public void addcatogery( Catogery catogery)
        {
            using(SqlConnection conn = new SqlConnection(con))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_add",conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
               
                cmd.Parameters.AddWithValue("@productname",catogery.ProductName);
                cmd.Parameters.AddWithValue("@catogeryid", catogery.CategoryId);
                cmd.Parameters.AddWithValue("@catogeryname", catogery.catogeryName);
                cmd.ExecuteNonQuery();
                conn.Close();


            }
        }


         public Catogery Getca(int?id)
        {
          Catogery cato = new Catogery();
            using(SqlConnection conn = new SqlConnection(con))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select * from Product where Product_Id="+Convert.ToInt32(id)+"", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    
                    cato.ProductId = Convert.ToInt32(dr[0]);
                    cato.ProductName = dr[1].ToString();
                    cato.CategoryId = Convert.ToInt32(dr[2]);
                    cato.catogeryName = dr[3].ToString();
                   
                }

                return cato; 

            }
        }

        public void updatecatogery( Catogery catogery)
        {
            using (SqlConnection conn = new SqlConnection(con))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_update", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@productid", catogery.ProductId);
                cmd.Parameters.AddWithValue("@productname", catogery.ProductName);
                cmd.Parameters.AddWithValue("@catogeryid", catogery.CategoryId);
                cmd.Parameters.AddWithValue("@catogeryname", catogery.catogeryName);
                cmd.ExecuteNonQuery();
                conn.Close();


            }
        }
        public void update(Catogery ca,int? Id)
        {
            using (SqlConnection conn = new SqlConnection(con))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("update Product set '"+ca.ProductName+"',"+ca.CategoryId+",'"+ca.catogeryName+"' where Product_Id="+Id+" ", conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void deleteca(Catogery ca)
        {
            using (SqlConnection conn = new SqlConnection(con))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_delete", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@productid", ca.ProductId);
                cmd.ExecuteNonQuery();
                conn.Close ();  

            }
        }

    }
}
